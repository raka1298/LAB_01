# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-26 06:51
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 9, 26, 13, 51, 46, 885591, tzinfo=utc)),
        ),
    ]
